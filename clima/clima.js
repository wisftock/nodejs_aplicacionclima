const axios = require('axios');
const { API_KEY } = require('../api/api');
// const
const getClima = async (lat, log) => {
  try {
    const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${log}&appid=${API_KEY}&units=metric`;

    const response = await axios.get(url);
    const temperatura = response.data.main.temp;
    return {
      temperatura,
    };
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getClima,
};
