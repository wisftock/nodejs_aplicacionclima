const city = require('./city/city');
const clima = require('./clima/clima');
const argv = require('yargs').options({
  direccion: {
    alias: 'd',
    desc: 'Descripcion de la ciudad para obtener un clima',
    demand: true,
  },
}).argv;

// city
//   .getLatLong(argv.direccion)
//   .then((result) => console.log(result))
//   .catch((err) => console.log(err));

// clima
//   .getClima(-12.04, -77.03)
//   .then((result) => console.log(result))
//   .catch((err) => console.log(err));

const getInfor = async (direccion) => {
  try {
    const coordenada = await city.getLatLong(direccion);
    const temp = await clima.getClima(coordenada.lat, coordenada.lon);
    return `El clima de ${direccion} es de ${temp.temperatura}`;
  } catch (error) {
    return `No se pudo determinar el clima de ${direccion}`;
  }
};

getInfor(argv.direccion)
  .then((result) => console.log(result))
  .catch((err) => console.log(err));
