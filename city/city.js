const axios = require('axios');
const { API_KEY } = require('../api/api');
const getLatLong = async (direccion) => {
  try {
    const city = encodeURI(direccion);

    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`;
    const response = await axios.get(url);
    // console.log(response.data);
    const coordenada = response.data.coord;
    const nombre = response.data.name;
    const lat = coordenada.lat;
    const lon = coordenada.lon;
    return {
      nombre,
      lat,
      lon,
    };
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getLatLong,
};
